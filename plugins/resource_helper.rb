# frozen_string_literal: true

require_relative '../lib/resource_helper'

Gitlab::Triage::Resource::Context.include ResourceHelper
