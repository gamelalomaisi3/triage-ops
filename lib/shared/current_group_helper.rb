# frozen_string_literal: true

require_relative 'resource_group'

module CurrentGroupHelper
  def current_group
    @current_group ||= ResourceGroup.new(current_group_name, label_names)
  end

  def current_group_em
    current_group.engineering_manager
  end

  def current_group_frontend_em
    current_group.frontend_engineering_manager
  end

  def current_group_backend_em
    current_group.backend_engineering_manager
  end

  def current_group_set
    current_group.software_engineer_in_test
  end

  def current_group_pm
    current_group.product_manager
  end

  def current_group_pd
    current_group.product_designer
  end
end
